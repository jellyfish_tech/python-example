import hashlib
import os

from flask import jsonify, request, session
from flask.views import MethodView
from redis import StrictRedis
from werkzeug.utils import secure_filename

from helpers import get_random_string, carpe_login_required
from models import *
from database import db_session
from api.base import ListResource, DetailResource, Api, JsonResponseMixin, JsonDataMixin
from flask_restful import Resource

MEDIA_URL = '/media/'


class AuthAPI(Api, JsonResponseMixin, JsonDataMixin):

    def check_password(self, user, password):
        hash_password = AdminUserManager().hash_password(password)

        return user.password == hash_password

    def post(self):
        data = self.get_data()

        if not data.get("username", False) or not data.get("password", False):
            return self.build_response({
                "error": "Username or Password not sent"
                }, 400)

        user = AdminUser.query.filter_by(username=data.get('username')).first()

        if user is None:
            return self.build_response({
                "error": "Username not found"
                }, 404)

        if self.check_password(user, data.get('password')):
            session.clear()
            session_key = get_random_string()
            session['session_key'] = session_key
            session[session_key] = {
                    "user": user.get_data()
                    }
            return self.build_response({
                "session_key": session_key,
                "user": user.get_data()
                })
        else:
            return self.build_response({
                "error": "Password is wrong"
                }, 401)

    def delete(self):
        session.clear()

        return self.build_response({}, 204)


class GoalsAPI(ListResource):
    model = Goal


class GoalsDetailsAPI(Api, JsonResponseMixin, JsonDataMixin):
    def get(self, id):
        goal = Goal.query.get(id)

        if goal is None:
            return self.build_response({
                "error": "Goal not found"
            }, 404)

        return self.build_response(goal.get_data())


    def delete(self, id):
        goal = Goal.query.get(id)

        if goal is None:
            return self.build_response({
                "error": "Goal not found"
            }, 404)

        db_session.delete(goal)
        db_session.commit()

        return self.build_response({}, 204)

    def put(self, id):
        data = self.get_data()
        goal = Goal.query.filter_by(id=id)
        db_session.begin()
        goal.update(data)
        db_session.commit()

        return self.build_response(goal.first().get_data())



class TaskDetailsAPI(DetailResource):
    model = Task


class GoalsTasksAPI(JsonResponseMixin, Api):
    def get(self, id):
        tasks = Task.query.filter_by(goal_id=id)
        response = [task.get_data() for task in tasks]
        return self.build_response(response)


class TasksAPI(ListResource):
    model = Task

class CampaignAPI(ListResource):
    """CampaignAPI"""
    model = Campaign


class CampaignInfluencersAPI(JsonResponseMixin, Api):
    def get(self, campaign_id):
        campaign = Campaign.query.get(campaign_id)

        if campaign is None:
            return self.build_response({
                "error": "Campaign not found"
            }, 404)

        influencers = campaign.influencers.all()

        return self.build_response({
            "campaign": campaign.get_data(),
            "influencers": [influencer.get_data() for influencer in influencers]
        })


class InfluencersAPI(ListResource):
    model = Influencer

    def post(self):
        db_session.begin()
        data = self.get_data()
        campaign_id = data.get("campaign")
        influencer = Influencer(user_id=data.get("user_id"))
        db_session.add(influencer)
        db_session.commit()
        db_session.begin()
        redis = StrictRedis(db=1)
        campaign_influencer = CampaignInfluencer(campaign_id=campaign_id, influencer_id=influencer.id)

        db_session.add(campaign_influencer)
        hash = hashlib.sha512("{0}".format(campaign_influencer.id)).hexdigest()
        if not data.get('short', True):
            campaign_influencer.uuid = hash[0:30]
        else:
            campaign_influencer.uuid = hash[0:5]

        db_session.commit()
        redis.set(campaign_influencer.uuid, campaign_influencer.get_campaign().get('target_url'))

        return self.build_response(influencer.get_data(), 201)


class InfluencerCampaignsAPI(Api, JsonResponseMixin):
    def get(self, influencer_id):
        influencer = Influencer.query.get(influencer_id)

        if influencer is None:
            return self.build_response({
                "error": "Influencer not found"
            }, 404)

        campaigns_influencers = CampaignInfluencer.query.filter_by(influencer_id=influencer.id)

        return self.build_response({
            "influencer": influencer.get_data(),
            "campaigns": [campaigns_influencer.get_data_influencers() for campaigns_influencer in campaigns_influencers]
        })


class TopicsAPI(ListResource):
    model = Topic

class QuestionsAPI(ListResource):
    model = Question

    def post(self):
        data = self.get_data()
        type = data.get('type')

        question = Question(type=data.get("type"), template=data.get('template'), name=data.get("name"),
                            topic_id=data.get("topic"))
        db_session.begin()
        if data.get("parent_id", False):
            question.parent_id = int(data.get("parent_id"))
            parent_question = Question.query.get(int(data.get("parent_id")))
            parent_question.has_child = True

        db_session.add(question)
        db_session.commit()

        if type == "choice":

            choices = data.get('choices').split(',')
            for choice_data in choices:
                db_session.begin()
                choice = QuestionChoice(value=choice_data, question_id=question.id)
                db_session.add(choice)
                db_session.commit()

        return self.build_response(question.get_data(), 201)


class QuestionDetailsAPI(DetailResource):
    model = Question

class QuestionTypesAPI(JsonResponseMixin, Api):
    def get(self):
        data = []
        for type in Question.TYPES:
            data.append({
                "type": type[0],
                "name": type[1],
            })
        return self.build_response(data)


class TopicQuestionsAPI(JsonResponseMixin, Api):
    def get(self, topic_id):
        topic = Topic.query.get(topic_id)

        if topic is None:
            return self.build_response({
                "error": "Topic not found"
            }, 404)

        questions = topic.questions.filter_by(parent_id=None)

        if request.args.get('no_childs', False):
            questions = topic.questions.filter_by(has_child=False)

        return self.build_response({
            "topic": topic.get_data(),
            "questions": [question.get_data() for question in questions]
        })


class UsersAPI(ListResource):
    model = CarpeUser


class IconsAPI(ListResource):
    model = Media
